package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Beans.UsuarioBean;
/**
 * Filtragem para a criação de Sessão e de redirecionamento de páginas
 * @author João Guilherme
 *
 */
public class LoginFilter implements Filter {

	@Override
	/**
	 * Método de verificação e criação de Sessão e Endereço de paginas
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req= (HttpServletRequest) request;
		HttpServletResponse res= (HttpServletResponse) response;
		UsuarioBean session=(UsuarioBean) req.getSession().getAttribute("UsuarioBean");
		String url= req.getRequestURI();
		if(session==null || !session.logado) {
			if(url.indexOf("texto.xhtml")>=0 || url.indexOf("sair.xhtml")>=0 || url.indexOf("imagem.xhtml")>=0 || url.indexOf("video.xhtml")>=0) {
				res.sendRedirect(req.getServletContext().getContextPath()+ "/faces/index.xhtml");
			}else {
				chain.doFilter(request, response);
			}
		}else{
			if(url.indexOf("index.xhtml")>=0) {
				res.sendRedirect(req.getServletContext().getContextPath()+ "/faces/texto.xhtml");
			}
			else if(url.indexOf("sair.xhtml")>=0){
				req.getSession().removeAttribute("UsuarioBean");
				res.sendRedirect(req.getServletContext().getContextPath()+ "/faces/index.xhtml");
			}else {
				chain.doFilter(request, response);
			}
		}
	}

}
