package Controle;

import java.sql.Connection;
import java.sql.SQLException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Modelo.Usuario;
public class ControleUsuario{
    public boolean adicionarUsuario(Usuario user) throws Exception{
        boolean resultado = false;
        try{
            Connection con = new Conexao().abrirConexao();
            Crypt cript= new Crypt();
            String key = "7134232f7e29792c5877475f2a68543779305c6f36685e707431203e63";
            byte[] senhaCript= cript.encrypt(user.getSenha(),key);
            String sql = "INSERT INTO usuario(nome,senha) VALUES(?,?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, user.getNome());
            ps.setBytes(2, senhaCript);
            if(!ps.execute()) {
                resultado = true;
            }
            new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return resultado;
    }

    public boolean logarUsuario(Usuario user) throws Exception{
    	boolean resultado=false;
        try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM usuario WHERE nome=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getNome());
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					String key = "7134232f7e29792c5877475f2a68543779305c6f36685e707431203e63";
					Crypt descrip= new Crypt();
					String senhaDescript= descrip.decrypt(rs.getBytes("senha"), key);
					if(user.getSenha().equals(senhaDescript)) {
						resultado = true;
					}
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
    }

    public boolean alterarSenha(Usuario user){
        boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE usuario SET senha=? WHERE id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, user.getId());
			ps.setString(2, user.getSenha());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
    }

	public boolean verificarEmail(String email){
		boolean resultado = false;
		try{
			Connection con=new Conexao().abrirConexao();
			String sql="SELECT count(id) AS c From usuario WHERE email=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, email);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				if(rs.getInt("c")==0) {
					resultado = true;
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}



}
