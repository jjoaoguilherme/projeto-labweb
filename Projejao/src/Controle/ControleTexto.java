package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Texto;

/** Classe controle para a manipula��o das informa��es de texto no Database
 * @author  Jo�o Guilherme
 * @version 1.0
 **/

public class ControleTexto {
	public boolean inserirTexto(Texto tex) {
		/**M�todo para inser��o de texto no banco de dados
		 * @param titulo - T�tulo para do texto
		 * @param texto - texto em sim
		 */
		boolean resultado = false;
		/**
		 * usando m�todo abrirConex�o para iniciar conex�o
		 */
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO texto(titulo,texto) VALUES(?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, tex.getTitulo());
			ps.setString(2, tex.getTexto());
			if(!ps.execute()) {
				resultado = true;
				/**
				 * usando m�todo fecharConexao para encerrar a conex�o
				 */
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public ArrayList<Texto> consultarTexto(){
		/**
		 * usando m�todo abrirConex�o para iniciar conex�o
		 * @param titulo - T�tulo para do texto
		 * @param texto - texto em si
		 * @param id
		 */
		ArrayList<Texto> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM texto;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Texto>();
				while(rs.next()) {
					Texto text = new Texto(rs.getInt("id"),rs.getString("titulo"),rs.getString("texto"));
					lista.add(text);
				}
			}
			/**
			 * usando m�todo fecharConexao para encerrar a conex�o
			 */
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean atualizarTexto(Texto tex,int id) {
		/**
		 * usando m�todo abrirConex�o para iniciar conex�o
		 * @param titulo - T�tulo para do texto
		 * @param texto - texto em si
		 * @param id- usado como chave prim�ria para editar no banco o texto requisitado
		 */
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE texto SET  titulo=?,texto=? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, tex.getTitulo());
			ps.setString(2, tex.getTexto());
			ps.setInt(3, id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public boolean deletarTexto(int id) {
		/**
		 * usando m�todo abrirConex�o para iniciar conex�o
		 */
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE FROM texto WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,id);
			if(!ps.execute()) {
				resultado = true;
			}
			/**
			 * usando m�todo fecharConexao para encerrar a conex�o
			 */
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	
	public Texto consultaUm(int id) {
		Texto text = null;
		try {
			/**
			 * @param titulo - T�tulo para do texto
			 * @param texto - texto em sim
			 * usando m�todo abrirConex�o para iniciar conex�o
			 */
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM texto WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				text = new Texto(rs.getInt("id"),rs.getString("texto"),rs.getString("titulo"));		
			}
			/**
			 * usando m�todo fecharConexao para encerrar a conex�o
			 */
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return text;
	}

}

