package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Imagem;;

/** Classe controle para a manipula��o das informa��es da imagem no Database
 *@author Jo�o Guilherme
 *@version 1.0
 *
**/

public class ControleImagem {
	public boolean inserirImagem(Imagem im) {
		/**M�todo para inser��o de imagem no banco de dados
		 * @param titulo - T�tulo para a imagem
		 * @param imagem - Caminho da imagem 
		 */
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO imagem(titulo,img) VALUES(?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, im.getTitulo());
			ps.setString(2, im.getImagem());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public ArrayList<Imagem> consultarImagem(){
		/**
		 * M�todo que consulta todas as imagens e seus respectivos atributos que est�o armazenados no banco
		 *  @param id - chave prim�ria de 
		 *  @param titulo - t�tulo de cada imagem
		 *  @param imagem - caminho de cada imagem
		 */
		ArrayList<Imagem> lista = new ArrayList<Imagem>();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM imagem;";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			if(rs != null) {
				while(rs.next()) {
					Imagem im = new Imagem();
					im.setId(rs.getInt("id"));
					im.setTitulo(rs.getString("titulo"));
					im.setImagem(rs.getString("img"));
					lista.add(im);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}
	

	public Imagem consultaUm(int id) {
		/**
		 * M�todo que retorna apenas uma imagem
		 * @param id - atributo da imagem que est� como chave prim�ria
		 */
		Imagem imagem = null;
		/**
		 * usando m�todo abrirConex�o para iniciar conex�o
		 */
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM imagem WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				imagem = new Imagem(rs.getInt("id"),rs.getString("titulo"),rs.getString("img"));		
			}
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return imagem;
	}
	
	public boolean editar(Imagem imagem) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE imagem SET titulo=?,img=? WHERE id=?");
			ps.setString(1,imagem.getTitulo());
			ps.setString(2,imagem.getImagem());
			ps.setInt(3, imagem.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o imagem: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}
	
	public boolean deletarImagem(int id) {
		/**
		 * M�todo para remover uma inser��o de imagem 
		 */
		boolean resultado = false;
		/**
		 * usando m�todo abrirConex�o para iniciar conex�o
		 */
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE FROM imagem WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,id);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	

}
