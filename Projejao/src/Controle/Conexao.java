package Controle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public final class Conexao {
	public Connection abrirConexao(){
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String servidor = "jdbc:mysql://localhost/jw";
			String usuario = "root";
			String senha = "123456";
			con = DriverManager.getConnection(servidor,usuario,senha);
		}catch(Exception e) {
			System.out.println("Problema ao conectar ao banco");
		}
		return con;
	}
	public void fecharConexao(Connection con) {
		try {
			con.close();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
