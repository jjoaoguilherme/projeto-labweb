package Modelo;

public class Texto {

	public int id;
	public String titulo;
	public String texto;
	
	public Texto(int id,String titulo,String texto) {
		this.setId(id);
		this.setTitulo(titulo);
		this.setTexto(texto);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
}
