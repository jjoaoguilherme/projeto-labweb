package Modelo;

public class Usuario {

	public int id;
	public String nome;
	public String senha;
	
	public Usuario(int id,String nome , String senha){
		this.setId(id);
		this.setNome(nome);
		this.setSenha(senha);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
