package Modelo;

public class Video {

	public int id;
	public String titulo;
	public String video;

	public Video(int id,String titulo,String video) {
	this.setId(id);
	this.setTitulo(titulo);
	this.setVideo(video);
}
	
	public Video() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
	
}
