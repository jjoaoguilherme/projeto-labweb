package Modelo;

public class Imagem {

	public int id;
	public String titulo;
	public String imagem;
	
	public Imagem(int id,String titulo,String imagem) {
		this.setId(id);
		this.setTitulo(titulo);
		this.setImagem(imagem);
	}
	
	public Imagem() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
}
