package Beans;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.servlet.http.Part;
import Controle.ControleVideo;
import Modelo.Video;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;

@ManagedBean(name="VideoBean")


public class VideoBean {

	
	public int id;
	public String titulo;
	public Part video;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public Part getVideo() {
		return video;
	}
	public void setVideo(Part video) {
		this.video = video;
	}
	
	public String inserir() throws SQLException, IOException{
		String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
		InputStream vid = video.getInputStream();
		String vidNome = video.getSubmittedFileName();
		int vidDiv = vidNome.lastIndexOf('.');
		String ext = vidNome.substring(vidDiv);
		vidNome = titulo + ext;
		Files.copy(vid, new File(realPath+"upVideo", vidNome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		Video videom = new Video(this.getId(),this.getTitulo(),"upVideo/"+vidNome);
		ControleVideo videoc= new ControleVideo();
		boolean resultado = videoc.inserirVideo(videom);
		if(resultado) {
			return "video.xhtml?faces-redirect=true;";
		}else {
			return "video.xhtml?faces-redirect=true;";
		}
	}
	
	/*
	public String editar() {
		ModeloVideo videom = new ModeloVideo(this.getId(),this.getTitulo(),this.getTexto());
		if(new ControleVideo().editar(videom)) {
			return "texto";	
		}else {
			return "index";
		}
	}
	*/
	public void carregarId(int id) {
		Video videom = new ControleVideo().consultaUm(id);
		this.setId(videom.getId());
		this.setTitulo(videom.getTitulo());
		
	}
	
	public boolean deletarVideo(Video id) {
		boolean resultado = new ControleVideo().deletarVideo(id.getId());
		return resultado;
	}
	
	
	ArrayList<Video> lista = new ControleVideo().consultarVideo();
	public ArrayList<Video> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Video> lista) {
		this.lista = lista;
	}
	public Video retornarItem(int id) {
		return lista.get(id);
	}

		
}
