package Beans;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import Controle.ControleTexto;
import Modelo.Texto;


@ManagedBean(name="TextoBean")

/**
 * 
 * @author Jo�o Guilherme
 *
 */

public class TextoBean {
	
	/**
	 *  @param id
	 *  @param titulo
	 *  @param texto
	 *  
	 */
		
	public int id;
	public String titulo;
	public String texto;
	
	/**
	 * Encapsulamento
	 */
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String inserir() throws SQLException{
		/**
		 * Requisita a classe Controle para inserir t�tulo e texto no banco
		 * @param id
		 * @param titulo
		 * @param texto
		 */
		Texto texto = new Texto(this.getId(),this.getTitulo(),this.getTexto());
		ControleTexto text= new ControleTexto();
		boolean resultado = text.inserirTexto(texto);
		if(resultado) {
			return "texto.xhtml?faces-redirect=true;";
		}else {
			return "texto.xhtml?faces-redirect=true;";
		}
	}
	public String editar() {
		/**
		 * Requisita  o m�todo editar da classe Controle 
		 * @param id
		 * @param titulo
		 * @param texto
		 */
		Texto text = new Texto(this.getId(),this.getTitulo(),this.getTexto());
		if(new ControleTexto().atualizarTexto(text, id)) {
			return "texto.xhtml?faces-redirect=true;";	
		}else {
			return "texto.xhtml?faces-redirect=true;";
		}
	}
	
	
	public boolean remover(Texto id) {
		/**
		 * M�todo que requisita o m�todo remover da classe Controle texto
		 * @param id - serve como chave prim�ria para remover apenas um
		 * @param titulo
		 * @param texto
		 */
		boolean resultado = new ControleTexto().deletarTexto(id.getId());
		return resultado;
	}
	
	
	ArrayList<Texto> lista = new ControleTexto().consultarTexto();
	public ArrayList<Texto> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Texto> lista) {
		this.lista = lista;
	}
	public Texto retornarItem(int id) {
		return lista.get(id);
	}
	
	public void carregarId(int id) {
		/**
		 * M�todo que requisita o m�todo consulta um da classe Controle de texto
		 * @param id
		 * @param titulo
		 * @param texto
		 */
		Texto text = new ControleTexto().consultaUm(id);
		this.setId(text.getId());
		this.setTitulo(text.getTitulo());
		this.setTexto(text.getTexto());
	}

}
	

