package Beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Controle.ControleUsuario;
import Modelo.Usuario;
@SessionScoped

@ManagedBean(name="UsuarioBean")

public class UsuarioBean {
	private int id;
	private String nome;
	private String senha;
	public boolean logado;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String inserir() throws Exception {
			Usuario usuario = new Usuario(this.getId(),this.getNome(),this.getSenha());
			ControleUsuario usuarioC= new ControleUsuario();
			boolean resultado = usuarioC.adicionarUsuario(usuario);
			if(resultado) {
				return "home.xhtml?faces-redirect=true;";
			}else {
				return "texto";
			}
	}
	public String verificarUsuario() throws Exception {
		Usuario usuario = new Usuario(this.getId(),this.getNome(),this.getSenha());
		ControleUsuario usuarioC= new ControleUsuario();
		boolean resultado = usuarioC.logarUsuario(usuario);
		if(resultado) {
			logado=true;
			return "texto.xhtml?faces-redirect=true;";
		}else {
			return "index.xhtml?faces-redirect=true;";
		}
	}
}
