package Beans;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.servlet.http.Part;
import Controle.ControleImagem;
import Modelo.Imagem;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
@ManagedBean(name="ImagemBean")

/**
 * 
 * @author Jo�o Guilherme
 *
 */

public class ImagemBean {
	
	/**
	 * Classe para manipular a p�gina de imagem
	 * @param  id 
	 * @param  titulo
	 * @param  imagem
	 */
	
	public int id;
	public String titulo;
	public Part imagem;
	public String diretorio;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Part getImagem() {
		return imagem;
	}
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	}
	
	public String getDiretorio() {
		return diretorio;
	}
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}
	public String inserir() throws SQLException, IOException{
		/**
		 * M�todo ligado com a classe ControleImagem, excuta o m�todo
		 * @param imgNome - pega o nome da imagem
		 * @parm imgDiv - pega a extens�o da imagem
		 */
		String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
		InputStream img = imagem.getInputStream();
		String imgNome = imagem.getSubmittedFileName();
		int imgDiv = imgNome.lastIndexOf('.');
		String ext = imgNome.substring(imgDiv);
		imgNome = titulo + ext;
		Files.copy(img, new File(realPath+"upImg", imgNome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		Imagem imagemm = new Imagem(this.getId(),this.getTitulo(),"upImg/"+imgNome);
		ControleImagem imagemc= new ControleImagem();
		boolean resultado = imagemc.inserirImagem(imagemm);
		if(resultado) {
			return "imagem.xhtml?faces-redirect=true;";
		}else {
			return "imagem.xhtml?faces-redirect=true;";
		}
	}
	
	public String editar() throws IOException {
		String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
		InputStream img = imagem.getInputStream();
		String imgNome = imagem.getSubmittedFileName();
		int imgDiv = imgNome.lastIndexOf('.');
		String ext = imgNome.substring(imgDiv);
		imgNome = titulo + ext;
		Files.copy(img, new File(realPath+"upImg", imgNome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		Imagem imagem = new Imagem(this.getId(),this.getTitulo(),"upImg/"+imgNome);
		if(new ControleImagem().editar(imagem)){
			return "imagem.xhtml?faces-redirect=true;";	
		}else {
			return "imagem.xhtml?faces-redirect=true;";
		}
	}
	
	
	public void carregarId(int id) {
		/**
		 * M�todo para carregar um registro de imagem espec�fico
		 * @param id- pega o id espec�fico da imagem requisitada
		 * @param titulo- pega o t�tulo espec�fico da imagem requisitada  
		 */
		Imagem imagem = new ControleImagem().consultaUm(id);
		this.setId(imagem.getId());
		this.setTitulo(imagem.getTitulo());
		this.setDiretorio(imagem.getImagem());
	}
	
	public boolean remover(Imagem id) {
		/**
		 * M�todo para remover imagem
		 * @param id est� sendo usado como chave prim�ria para excluir apenas uma imagem 
		 */
		boolean resultado = new ControleImagem().deletarImagem(id.getId());
		return resultado;
	}
	
	
	ArrayList<Imagem> lista = new ControleImagem().consultarImagem();
	public ArrayList<Imagem> getLista() {
		/**
		 * M�todo que carrega todas as imagens atrav�s inseridas no banco
		 */
		return lista;
	}
	public void setLista(ArrayList<Imagem> lista) {
		this.lista = lista;
	}
	public Imagem retornarItem(int id) {
		return lista.get(id);
	}

	
}
